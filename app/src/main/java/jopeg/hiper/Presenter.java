package jopeg.hiper;


import android.support.annotation.NonNull;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class Presenter {

    Single<String> sendText(String text) {
        return Single.create(emitter -> {
            Call<String> call = ApiImpl.newCall().sendText(text);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call,
                                       @NonNull Response<String> response) {
                    emitter.onSuccess(response.body());
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    emitter.onError(t);
                }
            });
        });
    }
}
