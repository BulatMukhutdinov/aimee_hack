package jopeg.hiper;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

class ApiImpl {
    private static final long REQUEST_TIMEOUT = 30;

    private static ApiImpl instance;

    private final String serverUrl;

    private ApiImpl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    static void init(String serverUrl) {
        instance = new ApiImpl(serverUrl);
    }

    static Api newCall() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original
                    .newBuilder()
                    .build();
            return chain.proceed(request);
        })
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .followRedirects(false)
                .followSslRedirects(false)
                .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(instance.serverUrl)
                        .addConverterFactory(ScalarsConverterFactory.create());

        Retrofit retrofit = builder
                .client(httpClient.build())
                .build();
        return retrofit.create(Api.class);
    }
}