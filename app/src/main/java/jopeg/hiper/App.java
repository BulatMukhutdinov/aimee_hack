package jopeg.hiper;


import android.app.Application;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

public class App extends Application {
    private static final String URL = "http://10.17.0.98:8080/";

    @Override
    public void onCreate() {
        super.onCreate();

        ApiImpl.init(URL);
        Logger.addLogAdapter(new AndroidLogAdapter());
    }
}
