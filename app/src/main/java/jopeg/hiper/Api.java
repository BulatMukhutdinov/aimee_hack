package jopeg.hiper;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Api {

    @POST("/monitoring/read")
    Call<String> sendText(@Body String text);
}
