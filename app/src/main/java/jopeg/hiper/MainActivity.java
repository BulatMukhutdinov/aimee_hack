package jopeg.hiper;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.orhanobut.logger.Logger;

import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import jopeg.hiper.databinding.ActivityMainBinding;
import ru.yandex.speechkit.Emotion;
import ru.yandex.speechkit.Error;
import ru.yandex.speechkit.Language;
import ru.yandex.speechkit.OnlineModel;
import ru.yandex.speechkit.OnlineRecognizer;
import ru.yandex.speechkit.OnlineVocalizer;
import ru.yandex.speechkit.Recognition;
import ru.yandex.speechkit.Recognizer;
import ru.yandex.speechkit.RecognizerListener;
import ru.yandex.speechkit.SpeechKit;
import ru.yandex.speechkit.Synthesis;
import ru.yandex.speechkit.Track;
import ru.yandex.speechkit.Vocalizer;
import ru.yandex.speechkit.VocalizerListener;
import ru.yandex.speechkit.Voice;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;


public class MainActivity extends AppCompatActivity implements RecognizerListener, VocalizerListener {
    private static final String API_KEY_FOR_TESTS_ONLY = "069b6659-984b-4c5f-880e-aaedcfd84102";
    private static final int REQUEST_PERMISSION_CODE = 31;

    private LiveDataTimerViewModel mLiveDataTimerViewModel;

    private ActivityMainBinding binding;

    private Recognizer recognizer;
    private Vocalizer vocalizer;

    private Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mLiveDataTimerViewModel = ViewModelProviders.of(this).get(LiveDataTimerViewModel.class);
        presenter = new Presenter();

        try {
            SpeechKit.getInstance().init(this, API_KEY_FOR_TESTS_ONLY);
            SpeechKit.getInstance().setUuid(UUID.randomUUID().toString());
        } catch (SpeechKit.LibraryInitializationException ignored) {
            //do not ignore in a prod version!
        }

        recognizer = new OnlineRecognizer.Builder(Language.RUSSIAN, OnlineModel.DIALOG,
                MainActivity.this).setSilenceBetweenUtterances(500, TimeUnit.MILLISECONDS).build();
        vocalizer = new OnlineVocalizer.Builder(Language.RUSSIAN, this)
                .setVoice(Voice.OKSANA)
                .setEmotion(Emotion.GOOD)
                .setAutoPlay(true)
                .build();

        binding.talk.setOnClickListener(v -> startRecognizer());
        subscribe();

        presenter.sendText("start")
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(answer -> vocalizer.synthesize(answer, Vocalizer.TextSynthesizingMode.INTERRUPT),
                        throwable -> Logger.e(throwable.getMessage()));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        vocalizer.cancel();
        vocalizer.destroy();
        vocalizer = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != REQUEST_PERMISSION_CODE) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length == 1 && grantResults[0] == PERMISSION_GRANTED) {
            startRecognizer();
        } else {
            Toast.makeText(this, "Record audio permission was not granted", Toast.LENGTH_LONG).show();
        }
    }

    private void startRecognizer() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, RECORD_AUDIO) != PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{RECORD_AUDIO}, REQUEST_PERMISSION_CODE);
        } else {
            recognizer.startRecording();
        }
    }

    @Override
    public void onRecordingBegin(@NonNull Recognizer recognizer) {

    }

    @Override
    public void onSpeechDetected(@NonNull Recognizer recognizer) {

    }

    @Override
    public void onSpeechEnds(@NonNull Recognizer recognizer) {

    }

    @Override
    public void onRecordingDone(@NonNull Recognizer recognizer) {

    }

    @Override
    public void onPowerUpdated(@NonNull Recognizer recognizer, float v) {

    }

    @Override
    public void onPartialResults(@NonNull Recognizer recognizer, @NonNull Recognition recognition, boolean eOfU) {
        if (eOfU) {
            presenter.sendText(recognition.getBestResultText())
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(answer -> vocalizer.synthesize(answer, Vocalizer.TextSynthesizingMode.INTERRUPT),
                            throwable -> Logger.e(throwable.getMessage()));
        }
    }

    @Override
    public void onRecognitionDone(@NonNull Recognizer recognizer) {

    }

    @Override
    public void onRecognizerError(@NonNull Recognizer recognizer, @NonNull Error error) {

    }

    @Override
    public void onMusicResults(@NonNull Recognizer recognizer, @NonNull Track track) {

    }

    @SuppressWarnings("ConstantConditions")
    private void subscribe() {
        final Observer<Long> elapsedTimeObserver = time -> {
            String minutes = String.format(Locale.getDefault(), "%02d", time / 60);
            String seconds = String.format(Locale.getDefault(), "%02d", time % 60);

            binding.timer.setText(getString(R.string.time, minutes, seconds));
        };

        mLiveDataTimerViewModel.getElapsedTime().observe(this, elapsedTimeObserver);
    }

    @Override
    public void onSynthesisDone(@NonNull Vocalizer vocalizer) {

    }

    @Override
    public void onPartialSynthesis(@NonNull Vocalizer vocalizer, @NonNull Synthesis synthesis) {

    }

    @Override
    public void onPlayingBegin(@NonNull Vocalizer vocalizer) {

    }

    @Override
    public void onPlayingDone(@NonNull Vocalizer vocalizer) {

    }

    @Override
    public void onVocalizerError(@NonNull Vocalizer vocalizer, @NonNull Error error) {

    }
}
